enum ConversationServiceTypeError: Error {
  
}

protocol ConversationServiceType {
  func downloadAllConversations() -> Observable<Result<Conversation, APIError>>
  //func downloadAllConversations() -> Observable<[Conversation]>
}

class ConversationService: ConversationServiceType {
  
  func downloadAllConversations() -> Observable<Result<Conversation, APIError>> {
    return Observable.create { observer in
      ConversationAPI.downloadAllConversations(completion: { conversationOrNil, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        } else {
          observer.onNext(.success(conversationOrNil!))
        }
      })
      return Disposables.create()
    }
  }
  
//  func downloadAllConversations() -> Observable<[Conversation]> {
//    return Observable.create { observer in
//      ConversationAPI.downloadAllConversations(completion: { conversations, errorOrNil in
//        observer.onNext(conversations)
//        observer.onCompleted()
//      })
//      return Disposables.create()
//    }
//  }
  
}
