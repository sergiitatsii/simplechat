enum MessageServiceTypeError: Error {
  
}

protocol MessageServiceType {
  func downloadAllMessages(for userID: String) -> Observable<Result<Message, APIError>>
  func markMessagesAsRead(for userID: String) -> Observable<Result<Bool, APIError>>
  func send(message: Message, to userID: String) -> Observable<Result<Bool, APIError>>
//  func downloadAllMessages(for userID: String) -> Observable<Message?>
//  func markMessagesAsRead(for userID: String) -> Observable<Bool>
//  func send(message: Message, to userID: String) -> Observable<Bool>
}

class MessageService: MessageServiceType {
  
  func downloadAllMessages(for userID: String) -> Observable<Result<Message, APIError>> {
    return Observable.create { observer in
      MessageAPI.downloadAllMessages(for: userID, completion: { messageOrNil, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        } else {
          observer.onNext(.success(messageOrNil!))
        }
      })
      return Disposables.create()
    }
  }

  func markMessagesAsRead(for userID: String) -> Observable<Result<Bool, APIError>> {
    return Observable.create { observer in
      MessageAPI.markMessagesAsRead(for: userID, completion: { result, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        }
        observer.onNext(.success(result))
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }

  func send(message: Message, to userID: String) -> Observable<Result<Bool, APIError>> {
    return Observable.create { observer in
      MessageAPI.send(message: message, to: userID, completion: { result, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        }
        observer.onNext(.success(result))
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
//  func downloadAllMessages(for userID: String) -> Observable<Message?> {
//    return Observable.create { observer in
//      MessageAPI.downloadAllMessages(for: userID, completion: { messageOrNil, errorOrNil in
//        observer.onNext(messageOrNil)
//      })
//      return Disposables.create()
//    }
//  }
//
//  func markMessagesAsRead(for userID: String) -> Observable<Bool> {
//    return Observable.create { observer in
//      MessageAPI.markMessagesAsRead(for: userID, completion: { result, errorOrNil in
//        observer.onNext(result)
//        observer.onCompleted()
//      })
//      return Disposables.create()
//    }
//  }
//
//  func send(message: Message, to userID: String) -> Observable<Bool> {
//    return Observable.create { observer in
//      MessageAPI.send(message: message, to: userID, completion: { result, errorOrNil in
//        observer.onNext(result)
//        observer.onCompleted()
//      })
//      return Disposables.create()
//    }
//  }
  
}
