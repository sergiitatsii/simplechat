enum UserServiceTypeError: Error {
  
}

protocol UserServiceType {
  func register(with name: String, email: String, password: String, avatar: UIImage) -> Observable<Result<Bool, APIError>>
  //func register(with name: String, email: String, password: String, avatar: UIImage) -> Observable<Bool>
  //func login(with email: String, password: String) -> Observable<Result<Bool, APIError>>
  func login(with email: String, password: String) -> Observable<Bool>
  func logout() -> Observable<Bool>
  func info(for userID: String) -> Observable<User?>
  func downloadAllUsers(except userID: String) -> Observable<Result<[User], APIError>>
  func checkUserVerification() -> Observable<Bool>
}

class UserService: UserServiceType {
  
  //static let shared = UserService()
  
  //fileprivate init() {}
  
  func register(with name: String, email: String, password: String, avatar: UIImage) -> Observable<Result<Bool, APIError>> {
    return Observable.create { observer in
      UserAPI.register(with: name, email: email, password: password, avatar: avatar, completion: { status, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        }
        observer.onNext(.success(status))
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
//  func register(with name: String, email: String, password: String, avatar: UIImage) -> Observable<Bool> {
//    return Observable.create { observer in
//      UserAPI.register(with: name, email: email, password: password, avatar: avatar, completion: { status, errorOrNil in
//        observer.onNext(status)
//        observer.onCompleted()
//      })
//      return Disposables.create()
//    }
//  }
  
  func login(with email: String, password: String) -> Observable<Bool> {
    return Observable.create({ observer -> Disposable in
      UserAPI.login(with: email, password: password, completion: { status, errorOrNil in
        observer.onNext(status)
        observer.onCompleted()
      })
      return Disposables.create()
    })
  }
  
//  func login(with email: String, password: String) -> Observable<Result<Bool, APIError>> {
//    return Observable.create({ observer -> Disposable in
//      UserAPI.login(with: email, password: password, completion: { status, errorOrNil in
//        if let error = errorOrNil {
//          observer.onNext(.failure(error))
//        }
//        observer.onNext(.success(status))
//        observer.onCompleted()
//      })
//      return Disposables.create()
//    })
//  }
  
  func logout() -> Observable<Bool> {
    return Observable.create { observer in
      UserAPI.logout(completion: { status, errorOrNil in
        observer.onNext(status)
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
  func info(for userID: String) -> Observable<User?> {
    return Observable.create { observer in
      UserAPI.info(for: userID, completion: { userOrNil, errorOrNil in
        observer.onNext(userOrNil)
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
  func downloadAllUsers(except userID: String) -> Observable<Result<[User], APIError>> {
    return Observable.create { observer in
      UserAPI.downloadAllUsers(except: userID, completion: { users, errorOrNil in
        if let error = errorOrNil {
          observer.onNext(.failure(error))
        }
        observer.onNext(.success(users))
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
  func checkUserVerification() -> Observable<Bool> {
    return Observable.create { observer in
      UserAPI.checkUserVerification(completion: { status in
        observer.onNext(status)
        observer.onCompleted()
      })
      return Disposables.create()
    }
  }
  
}
