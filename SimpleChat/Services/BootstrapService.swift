import Firebase
import IQKeyboardManagerSwift

enum BootstrapService {
  
  static func bootstrap() {
    AppAppearance.configure()
    FirebaseApp.configure()
    //IQKeyboardManager.shared.enable = true
  }
  
}
