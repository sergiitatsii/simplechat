class AppEnvironment {
  
  static let current = AppEnvironment()
  
  let userDefaults: UserDefaults
  let notificationCenter: NotificationCenter
  let debugService: DebugOutputProtocol
  
  init(
    userDefaults: UserDefaults = .standard,
    notificationCenter: NotificationCenter = .default,
    debugService: DebugOutputProtocol = PrintDebugOutputService())
  {
    self.userDefaults = userDefaults
    self.notificationCenter = notificationCenter
    self.debugService = debugService
  }
  
}
