import RxCoordinator
import Reachability

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  private let disposeBag = DisposeBag()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    BootstrapService.bootstrap()
    
    guard let window = self.window else { return false }
    window.backgroundColor = UIColor.SimpleChat.blue.withAlphaComponent(0.5)
    let router = InitialRouter(window: window)
    InitialRouter.shared = router
    router.setup()
    
    return true
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    Reachability.rx.reachable
      .filter { !$0 }
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { _ in
        guard let topVC = UIApplication.topViewController() else { return }
        InfoView.showIn(viewController: topVC, message: "No internet connection.")
      }, onError: {
        print("Reachability error: \($0.localizedDescription)")
      })
      .disposed(by: disposeBag)
  }
  
}
