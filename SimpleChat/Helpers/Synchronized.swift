/// Swift equivalent of @synchronized.

/// Or you can simply use instead:
///`let serialQueue = DispatchQueue(label: "com.test.mySerialQueue")
///serialQueue.sync {
///  ...
///}`
func synchronized(_ lock: Any, closure: () -> ()) {
  objc_sync_enter(lock)
  defer { objc_sync_exit(lock) }
  closure()
}
