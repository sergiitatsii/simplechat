import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

let StorageRef = Storage.storage().reference()
let DBRef = Database.database().reference()

enum Paths: String {
  case users
  case usersAvatars
  case credentials
  case userInformation
  case conversations
  case messagePictures
}

enum APIError: Error {
  case userCreationFailure(String)
  case userLoginFailure(String)
  case userLogoutFailure(String)
  case userInfoFailure(String)
  case downloadAllUsersFailure(String)
  case downloadAllMessagesFailure(String)
  case markMessagesAsReadFailure(String)
  case sendMessageFailure(String)
  case uploadMessageFailure(String)
  case downloadAllConversationsFailure(String)
//  case userNotAuthorized
//  case userNotExist
//  case noSharedConversations
//  case noMessagesInConversation
}

extension APIError {
  var localizedDescription: String {
    switch self {
    case let .userCreationFailure(message):
      return message
    case let .userLoginFailure(message):
      return message
    case let .userLogoutFailure(message):
      return message
    case let .userInfoFailure(message):
      return message
    case let .downloadAllUsersFailure(message):
      return message
    case let .downloadAllMessagesFailure(message):
      return message
    case let .markMessagesAsReadFailure(message):
      return message
    case let .sendMessageFailure(message):
      return message
    case let .uploadMessageFailure(message):
      return message
    case let .downloadAllConversationsFailure(message):
      return message
    }
  }
}

class UserAPI {
  
  class func register(with name: String, email: String, password: String, avatar: UIImage, completion: @escaping (Bool, APIError?) -> Void) {
    Auth.auth().createUser(withEmail: email, password: password, completion: { authResult, error in
      guard let user = authResult?.user else {
        completion(false, .userCreationFailure(error?.localizedDescription ?? ""))
        return
      }
      user.sendEmailVerification(completion: nil)
      
      guard let imageData = avatar.jpegData(compressionQuality: 0.1)  else {
        completion(false, .userCreationFailure("Problem generating image data."))
        return
      }
      let avatarRef = StorageRef.child(Paths.usersAvatars.rawValue).child(user.uid)
      avatarRef.putData(imageData, metadata: nil, completion: { metadata, error in
        guard let _ = metadata else {
          completion(false, .userCreationFailure(error?.localizedDescription ?? ""))
          return
        }
        avatarRef.downloadURL(completion: { url, error in
          guard let avatarURL = url else {
            completion(false, .userCreationFailure(error?.localizedDescription ?? ""))
            return
          }
          let values = ["name": name, "email": email, "avatarURL": avatarURL.absoluteString]
          DBRef.child(Paths.users.rawValue).child(user.uid).child(Paths.credentials.rawValue).updateChildValues(values, withCompletionBlock: { error, _ in
            if error == nil {
              let userInfo = ["email": email, "password": password]
              UserDefaults.standard.set(userInfo, forKey: Paths.userInformation.rawValue)
              completion(true, nil)
            }
          })
        })
      })
    })
  }
  
  class func login(with email: String, password: String, completion: @escaping (Bool, APIError?) -> Void) {
    Auth.auth().signIn(withEmail: email, password: password, completion: { authResult, error in
      guard let _ = authResult else {
        completion(false, .userLoginFailure(error?.localizedDescription ?? ""))
        return
      }
      let userInfo = ["email": email, "password": password]
      UserDefaults.standard.set(userInfo, forKey: Paths.userInformation.rawValue)
      completion(true, nil)
    })
  }
  
  class func logout(completion: @escaping (Bool, APIError?) -> Void) {
    do {
      try Auth.auth().signOut()
      UserDefaults.standard.removeObject(forKey: Paths.userInformation.rawValue)
      completion(true, nil)
    } catch {
      completion(false, .userLogoutFailure(error.localizedDescription))
    }
  }
  
  class func info(for userID: String, completion: @escaping (User?, APIError?) -> Void) {
    DBRef.child(Paths.users.rawValue).child(userID).child(Paths.credentials.rawValue).observeSingleEvent(of: .value, with: { credentialsSnapshot in
      guard
        let credentials = credentialsSnapshot.value as? [String : String],
        let name = credentials["name"],
        let email = credentials["email"],
        let url = credentials["avatarURL"]
      else {
        completion(nil, .userInfoFailure("User credentials not exist."))
        return
      }
      
      let avatarURL = URL(string: url) ?? NSURL() as URL
      let user = User(id: userID, name: name, email: email, avatarURL: avatarURL)
      completion(user, nil)
    }) { error in
      completion(nil, .userInfoFailure(error.localizedDescription))
    }
  }
  
  class func downloadAllUsers(except userID: String, completion: @escaping ([User], APIError?) -> Void) {
    var allUsers = [User]()
    
    DBRef.child(Paths.users.rawValue).observe(.value, with: { usersSnapshot in
      if usersSnapshot.exists() {
        for item in usersSnapshot.children {
          guard let userSnapshot = item as? DataSnapshot else {
            completion([], .downloadAllUsersFailure("Incorrect stored data format."))
            return
          }
          let id = userSnapshot.key
          
          if id != userID {
            guard
              let user = userSnapshot.value as? [String : Any],
              let credentials = user["credentials"] as? [String : String],
              let name = credentials["name"],
              let email = credentials["email"],
              let url = credentials["avatarURL"]
            else {
                completion([], .downloadAllUsersFailure("User credentials not exist."))
                return
            }
            
            let avatarURL = URL(string: url) ?? NSURL() as URL
            let newUser = User(id: id, name: name, email: email, avatarURL: avatarURL)
            allUsers.append(newUser)
          }
        }
        completion(allUsers, nil)
      } else {
        completion([], .markMessagesAsReadFailure("There are no users yet."))
      }
    }) { error in
      completion([], .downloadAllUsersFailure(error.localizedDescription))
    }
  }
  
  class func checkUserVerification(completion: @escaping (Bool) -> Void) {
    guard let currentUser = Auth.auth().currentUser else {
      completion(false)
      return
    }
    currentUser.reload(completion: { _ in
      let status = currentUser.isEmailVerified
      completion(status)
    })
  }
  
}
