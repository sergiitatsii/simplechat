import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class MessageAPI {
  
  class func downloadAllMessages(for userID: String, completion: @escaping (Message?, APIError?) -> Void) {
    guard let currentUserID = Auth.auth().currentUser?.uid else {
      completion(nil, .downloadAllMessagesFailure("User is not authorized."))
      return
    }
    let conversationRef = DBRef
      .child(Paths.users.rawValue)
      .child(currentUserID)
      .child(Paths.conversations.rawValue)
      .child(userID)
    
    conversationRef.observe(.value, with: { conversationSnapshot in
      guard
        let conversation = conversationSnapshot.value as? [String : String],
        let location = conversation["location"]
      else {
        completion(nil, .downloadAllMessagesFailure("There are no shared conversations."))
        return
      }
      let messagesRef = DBRef
        .child(Paths.conversations.rawValue)
        .child(location)
      
      messagesRef.observe(.childAdded, with: { messagesSnapshot in
        guard
          let receivedMessage = messagesSnapshot.value as? [String : Any],
          let messageType = receivedMessage["type"] as? String,
          let content = receivedMessage["content"] as? String,
          let fromID = receivedMessage["fromID"] as? String,
          let timestamp = receivedMessage["timestamp"] as? Int
        else {
          completion(nil, .downloadAllMessagesFailure("There are no messages in the conversation."))
          return
        }
        var type = MessageType.text
        switch messageType {
        case "image":
          type = .image
        case "location":
          type = .location
        default: break
        }
        
        if fromID == currentUserID {
          let message = OutcomingMessage(type: type, content: content, timestamp: timestamp, isRead: true)
          completion(message, nil)
        } else {
          let message = IncomingMessage(type: type, content: content, timestamp: timestamp, isRead: true)
          completion(message, nil)
        }
      }) { messagesError in
        completion(nil, .downloadAllMessagesFailure(messagesError.localizedDescription))
      }
    }) { conversationsError in
      completion(nil, .downloadAllMessagesFailure(conversationsError.localizedDescription))
    }
  }
  
  class func markMessagesAsRead(for userID: String, completion: @escaping (Bool, APIError?) -> Void) {
    guard let currentUserID = Auth.auth().currentUser?.uid else {
      completion(false, .markMessagesAsReadFailure("User is not authorized."))
      return
    }
    let conversationRef = DBRef
      .child(Paths.users.rawValue)
      .child(currentUserID)
      .child(Paths.conversations.rawValue)
      .child(userID)
      
      conversationRef.observeSingleEvent(of: .value, with: { conversationSnapshot in
        guard
          let conversation = conversationSnapshot.value as? [String : String],
          let location = conversation["location"]
        else {
          completion(false, .markMessagesAsReadFailure("There are no shared conversations."))
          return
        }
        let messagesRef = DBRef
          .child(Paths.conversations.rawValue)
          .child(location)
        
        messagesRef.observeSingleEvent(of: .value, with: { messagesSnapshot in
          if messagesSnapshot.exists() {
            for item in messagesSnapshot.children {
              if let msgSnapshot = item as? DataSnapshot,
                let message = msgSnapshot.value as? [String : Any],
                let fromID = message["fromID"] as? String,
                fromID != currentUserID
              {
                messagesRef
                  .child(msgSnapshot.key)
                  .child("isRead")
                  .setValue(true)
              }
            }
            completion(true, nil)
          } else {
            completion(false, .markMessagesAsReadFailure("There are no messages in the conversation."))
          }
        })
      })
  }
  
  class func send(message: Message, to userID: String, completion: @escaping (Bool, APIError?) -> Void) {
    guard let currentUserID = Auth.auth().currentUser?.uid else {
      completion(false, .sendMessageFailure("User is not authorized."))
      return
    }
    switch message.type {
    case .text:
      let values = ["type": "text",
                    "content": message.content,
                    "fromID": currentUserID,
                    "toID": userID,
                    "timestamp": message.timestamp,
                    "isRead": false]
      
      MessageAPI.uploadMessage(with: values, to: userID, completion: { status, errorOrNil in
        completion(status, errorOrNil)
      })
    case .image:
      guard
        let image = message.content as? UIImage,
        let imageData = image.jpegData(compressionQuality: 0.5)
      else {
        completion(false, .sendMessageFailure("Problem generating image data."))
        return
      }
      let child = UUID().uuidString
      let messagePicture = StorageRef
        .child(Paths.messagePictures.rawValue)
        .child(child)
      
      messagePicture.putData(imageData, metadata: nil, completion: { metadata, error in
        guard let _ = metadata else {
          completion(false, .sendMessageFailure(error?.localizedDescription ?? ""))
          return
        }
        messagePicture.downloadURL(completion: { url, error in
          guard let pictureURL = url else {
            completion(false, .userCreationFailure(error?.localizedDescription ?? ""))
            return
          }
          let values = ["type": "image",
                        "content": pictureURL.absoluteString,
                        "fromID": currentUserID,
                        "toID": userID,
                        "timestamp": message.timestamp,
                        "isRead": false] as [String : Any]
          
          MessageAPI.uploadMessage(with: values, to: userID, completion: { status, errorOrNil in
            completion(status, errorOrNil)
          })
        })
      })
    case .location:
      let values = ["type": "location",
                    "content": message.content,
                    "fromID": currentUserID,
                    "toID": userID,
                    "timestamp": message.timestamp,
                    "isRead": false]
      
      MessageAPI.uploadMessage(with: values, to: userID, completion: { status, errorOrNil in
        completion(status, errorOrNil)
      })
    }
  }
  
  private class func uploadMessage(with values: [String : Any], to userID: String, completion: @escaping (Bool, APIError?) -> Void) {
    guard let currentUserID = Auth.auth().currentUser?.uid else {
      completion(false, .uploadMessageFailure("User is not authorized."))
      return
    }
    let conversationRef = DBRef
      .child(Paths.users.rawValue)
      .child(currentUserID)
      .child(Paths.conversations.rawValue)
      .child(userID)
    
    conversationRef.observeSingleEvent(of: .value, with: { conversationSnapshot in
      if conversationSnapshot.exists(),
        let conversation = conversationSnapshot.value as? [String : String],
        let location = conversation["location"]
      {
        DBRef
          .child(Paths.conversations.rawValue)
          .child(location)
          .childByAutoId()
          .setValue(values, withCompletionBlock: { error, _ in
            if error == nil {
              completion(true, nil)
            } else {
              completion(false, .uploadMessageFailure(error?.localizedDescription ?? ""))
            }
        })
      } else {
        DBRef
          .child(Paths.conversations.rawValue)
          .childByAutoId()
          .childByAutoId()
          .setValue(values, withCompletionBlock: { error, reference in
            guard let location = reference.parent?.key else {
              completion(false, .uploadMessageFailure(error?.localizedDescription ?? ""))
              return
            }
            let values = ["location": location]
            DBRef
              .child(Paths.users.rawValue)
              .child(currentUserID)
              .child(Paths.conversations.rawValue)
              .child(userID)
              .updateChildValues(values)
            
            DBRef.child(Paths.users.rawValue)
              .child(userID)
              .child(Paths.conversations.rawValue)
              .child(currentUserID)
              .updateChildValues(values)
            
            completion(true, nil)
        })
      }
    })
  }
  
}
