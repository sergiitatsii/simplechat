import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class ConversationAPI {
  
  class func downloadAllConversations(completion: @escaping (Conversation?, APIError?) -> Void) {
    guard let currentUserID = Auth.auth().currentUser?.uid else {
      completion(nil, .downloadAllConversationsFailure("User is not authorized."))
      return
    }
    
    DBRef
      .child(Paths.users.rawValue)
      .child(currentUserID)
      .child(Paths.conversations.rawValue)
      .observe(.childAdded, with: { conversationSnapshot in
        if conversationSnapshot.exists() {
          guard
            let conversation = conversationSnapshot.value as? [String : String],
            let location = conversation["location"]
            else {
              completion(nil, .downloadAllConversationsFailure("There are no conversations."))
              return
          }
          let fromID = conversationSnapshot.key
          
          UserAPI.info(for: fromID, completion: { userOrNil, errorOrNil in
            guard let user = userOrNil else {
              completion(nil, .downloadAllConversationsFailure(errorOrNil?.localizedDescription ?? ""))
              return
            }
            
            DBRef
              .child(Paths.conversations.rawValue)
              .child(location)
              .observe(.value, with: { conversationSnapshot in
                guard
                  let lastMsgSnapshot = conversationSnapshot.children.allObjects.last as? DataSnapshot,
                  let lastMsg = lastMsgSnapshot.value as? [String : Any],
                  let content = lastMsg["content"],
                  let timestamp = lastMsg["timestamp"] as? Int,
                  let messageType = lastMsg["type"] as? String,
                  let fromID = lastMsg["fromID"] as? String,
                  let isRead = lastMsg["isRead"] as? Bool
                  else {
                    completion(nil, .downloadAllConversationsFailure("There are no messages in the conversation."))
                    return
                }
                
                var type = MessageType.text
                switch messageType {
                case "text":
                  type = .text
                case "image":
                  type = .image
                case "location":
                  type = .location
                default: break
                }
                
                var owner = MessageOwner.sender
                if currentUserID == fromID {
                  owner = .receiver
                }
                
                let message = Message(type: type, content: content, owner: owner, timestamp: timestamp, isRead: isRead)
                let conversation = Conversation(user: user, lastMessage: message)
                completion(conversation, nil)
              })
          })
        } else {
          completion(nil, .downloadAllConversationsFailure("There are no conversations."))
          return
        }
      })
  }
    
}
