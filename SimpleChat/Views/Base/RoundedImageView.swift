class RoundedImageView: UIImageView {
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.layer.cornerRadius = self.bounds.size.width / 2.0
    self.clipsToBounds = true
  }
  
}
