final class RoundedBorderBarButtonItem: UIBarButtonItem {
  
  var button = UIButton(type: .system)
  
  override var image: UIImage? {
    get {
      return button.image(for: .normal)
    }
    set {
      var roundedImage = newValue
      if let image = newValue {
        let contentSize = CGSize(width: button.bounds.size.width,
                                 height: button.bounds.size.height)
        UIGraphicsBeginImageContextWithOptions(contentSize, false, 0.0)
        _ = UIBezierPath(roundedRect: CGRect(origin: CGPoint.zero, size: contentSize),
                         cornerRadius: button.bounds.size.width / 2).addClip()
        image.draw(in: CGRect(origin: CGPoint.zero, size: contentSize))
        if let finalImage = UIGraphicsGetImageFromCurrentImageContext() {
          roundedImage = finalImage
        }
        UIGraphicsEndImageContext()
      }
      button.setImage(roundedImage?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
  }
  
  init(radius: Int,
       borderWidth: Float = 2.0,
       borderColor: UIColor = UIColor.white,
       image: UIImage?,
       target: Any?,
       action: Selector?)
  {
    let button = UIButton(type: .system)
    button.frame = CGRect(x: 0, y: 0, width: radius * 2 , height: radius * 2)
    button.layer.cornerRadius = button.bounds.size.width / 2
    button.layer.borderWidth = CGFloat(borderWidth)
    button.layer.borderColor = borderColor.cgColor
    button.clipsToBounds = true
    button.imageView?.contentMode = .scaleAspectFill
    button.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
    if let action = action {
      button.addTarget(target, action: action, for: .touchUpInside)
    }
    
    self.init(customView: button)
    self.button = button
  }
  
  private override init() {
    super.init()
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
