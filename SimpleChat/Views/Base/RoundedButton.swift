class RoundedButton: UIButton {
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.layer.cornerRadius = self.bounds.size.height / 2.0
    self.clipsToBounds = true
  }
  
}
