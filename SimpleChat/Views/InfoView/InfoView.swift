fileprivate let viewPadding: CGFloat = 12.0
fileprivate let viewMinimalHeight: CGFloat = 50.0
fileprivate let animationDuration: TimeInterval = 0.3

class InfoView: UIView {
  
  @IBOutlet weak var textLabel: UILabel!
  @IBOutlet weak var closeButton: UIButton!
  
  private static var sharedView: InfoView!
  
  static func loadFromNib() -> InfoView {
    let nib = UINib(nibName: String(describing: InfoView.self), bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as! InfoView
  }
  
  static func showIn(viewController: UIViewController, message: String) {
    var displayVC = viewController
    
    if let tabController = viewController as? UITabBarController {
      displayVC = tabController.selectedViewController ?? viewController
    }
    
    if sharedView == nil {
      sharedView = loadFromNib()
      
      sharedView.layer.masksToBounds = false
      sharedView.layer.shadowColor = UIColor.darkGray.cgColor
      sharedView.layer.shadowOpacity = 1
      sharedView.layer.shadowOffset = CGSize(width: 0, height: 3)
    }
    
    sharedView.textLabel.text = message
    if sharedView?.superview == nil {
      displayVC.view.addSubview(sharedView)
      sharedView.translatesAutoresizingMaskIntoConstraints = false
      sharedView.leadingAnchor.constraint(equalTo: displayVC.view.leadingAnchor, constant: viewPadding).isActive = true
      sharedView.trailingAnchor.constraint(equalTo: displayVC.view.trailingAnchor, constant: -viewPadding).isActive = true
      sharedView.bottomAnchor.constraint(equalTo: displayVC.view.bottomAnchor, constant: -viewPadding).isActive = true
      sharedView.heightAnchor.constraint(greaterThanOrEqualToConstant: viewMinimalHeight).isActive = true
      sharedView.alpha = 0.0
      sharedView.fadeIn()
      sharedView.perform(#selector(fadeOut), with: nil, afterDelay: 3.0)
    }
  }
  
  @IBAction func closePressed(_ sender: UIButton) {
    fadeOut()
  }
  
  // MARK: Animations
  func fadeIn() {
    UIView.animate(withDuration: animationDuration, animations: {
      self.alpha = 1.0
    })
  }
  
  @objc func fadeOut() {
    NSObject.cancelPreviousPerformRequests(withTarget: self)

    UIView.animate(withDuration: animationDuration, animations: {
      self.alpha = 0.0
    }, completion: { _ in
      self.removeFromSuperview()
    })
  }
  
}
