import NVActivityIndicatorView

class LoadingIndicator: LoadableView {
  
  @IBOutlet weak var activityView: NVActivityIndicatorView!
  
  override var isHidden: Bool { didSet { updateAnimation() } }
  
  override func setupNib() {
    super.setupNib()
    layoutIfNeeded()
    updateAnimation()
  }
  
  func updateAnimation() {
    if isHidden { activityView?.stopAnimating() }
    else { activityView?.startAnimating() }
  }
  
}
