struct Conversation: Equatable {
  let user: User
  var lastMessage: Message
  
  // MARK: - Equatable
  static func ==(lhs: Conversation, rhs: Conversation) -> Bool {
    return lhs.user.id == rhs.user.id
  }
  
}
