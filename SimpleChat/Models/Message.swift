enum MessageType {
  case text
  case image
  case location
}

enum MessageOwner {
  case sender
  case receiver
}

class Message {
  var type: MessageType
  var content: Any
  var owner: MessageOwner
  var timestamp: Int
  var isRead: Bool
  var image: UIImage?
  private var toID: String?
  private var fromID: String?
  
  init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int, isRead: Bool) {
    self.type = type
    self.content = content
    self.owner = owner
    self.timestamp = timestamp
    self.isRead = isRead
  }
}

final class IncomingMessage: Message {
  init(type: MessageType, content: Any, timestamp: Int, isRead: Bool) {
    super.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: isRead)
  }
}

final class OutcomingMessage: Message {
  init(type: MessageType, content: Any, timestamp: Int, isRead: Bool) {
    super.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: isRead)
  }
}
