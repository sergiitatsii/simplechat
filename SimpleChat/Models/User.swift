class User: Equatable {
  let id: String
  let name: String
  let email: String
  var avatarURL: URL
  var avatar: UIImage?
  
  init(id: String, name: String, email: String, avatarURL: URL) {
    self.id = id
    self.name = name
    self.email = email
    self.avatarURL = avatarURL
  }
  
  // MARK: - Equatable
  static func ==(lhs: User, rhs: User) -> Bool {
    return lhs.id == rhs.id
  }
  
}
