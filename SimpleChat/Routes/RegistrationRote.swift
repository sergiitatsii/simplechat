enum RegistrationRote: Route {
  case registartion
  case login
  case conversations
  case alert(title: String, message: String)
  
  func prepareTransition(coordinator: AnyCoordinator<RegistrationRote>) -> NavigationTransition {
    switch self {
    case .registartion:
      guard var vc = RegistrationViewController.instantiate(storyboardName: "Login", initial: false) else { return .none() }
      let viewModel = RegistrationViewModel(coordinator: coordinator)
      vc.bind(to: viewModel)
      return .push(vc)
    case .login:
      let animation = Animation(presentationAnimation: nil, dismissalAnimation: CustomPresentations.flippingFromRightPresentation)
      return .dismiss(animation: animation)
    case .conversations:
      let coordinator = BasicCoordinator<ConversationsRoute>(initialRoute: .conversations)
      let animation = Animation(presentationAnimation: CustomPresentations.fadePresentation, dismissalAnimation: nil)
      return .present(coordinator, animation: animation)
    case let .alert(title, message):
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
      return .present(alert)
    }
  }
  
}

