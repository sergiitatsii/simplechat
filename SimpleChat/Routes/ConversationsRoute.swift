import CoreLocation

enum ConversationsRoute: Route {
  case conversations
  case userProfile
  case contacts
  case chat(with: User)
  case imagePreview(with: UIImage)
  case locationPreview(with: CLLocationCoordinate2D)
  case alert(title: String, message: String)
  case close
  case logout

  func prepareTransition(coordinator: AnyCoordinator<ConversationsRoute>) -> NavigationTransition {
    switch self {
    case .conversations:
      guard var vc = ConversationsViewController.instantiateFromXib() else { return .none() }
      let viewModel = ConversationsViewModel(coordinator: coordinator)
      vc.bind(to: viewModel)
      return .push(vc)
    case .userProfile:
      guard var vc = ProfileViewController.instantiateFromXib() else { return .none() }
      let viewModel = ProfileViewModel(coordinator: coordinator)
      vc.bind(to: viewModel)
      return .present(vc)
    case .contacts:
      guard var vc = ContactsViewController.instantiateFromXib() else { return .none() }
      let viewModel = ContactsViewModel(coordinator: coordinator)
      vc.bind(to: viewModel)
      return .present(vc)
    case let .chat(user):
      guard var vc = ChatViewController.instantiateFromXib() else { return .none() }
      let viewModel = ChatViewModel(coordinator: coordinator, user: user)
      vc.bind(to: viewModel)
      return .push(vc)
    case let .imagePreview(image):
      guard var vc = ImagePreviewViewController.instantiateFromXib() else { return .none() }
      let viewModel = ImagePreviewViewModel(coordinator: coordinator, image: image)
      vc.bind(to: viewModel)
      return .present(vc)
    case let .locationPreview(location):
      guard var vc = LocationPreviewViewController.instantiateFromXib() else { return .none() }
      let viewModel = LocationPreviewViewModel(coordinator: coordinator, location: location)
      vc.bind(to: viewModel)
      return .present(vc)
    case let .alert(title, message):
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
      return .present(alert)
    case .close:
      coordinator.viewController.dismiss(animated: true, completion: nil)
      return .none()
    case .logout:
      if let presentingViewController = coordinator.viewController.presentingViewController {
        presentingViewController.dismiss(animated: false) {
          InitialRouter.shared.setup()
        }
      } else {
        coordinator.viewController.dismiss(animated: false) {
          InitialRouter.shared.setup()
        }
      }
      return .none()
    }
  }
  
}
