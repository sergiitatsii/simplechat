import FirebaseAuth

enum LoginRoute: Route {
  case login
  case registration
  case conversations
  case alert(title: String, message: String)
  
  func prepareTransition(coordinator: AnyCoordinator<LoginRoute>) -> NavigationTransition {
    switch self {
    case .login:
      guard var vc = LoginViewController.instantiate(storyboardName: "Login", initial: false) else { return .none() }
      let viewModel = LoginViewModel(coordinator: coordinator)
      vc.bind(to: viewModel)
      return .push(vc)
    case .registration:
      let coordinator = BasicCoordinator<RegistrationRote>(initialRoute: .registartion)
      let animation = Animation(presentationAnimation: CustomPresentations.flippingFromLeftPresentation, dismissalAnimation: nil)
      return .present(coordinator, animation: animation)
    case .conversations:
      let coordinator = BasicCoordinator<ConversationsRoute>(initialRoute: .conversations)
      let animation = Animation(presentationAnimation: CustomPresentations.fadePresentation, dismissalAnimation: nil)
      return .present(coordinator, animation: animation)
    case let .alert(title, message):
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: nil))
      return .present(alert)
    }
  }
  
}

class InitialRouter {
  
  static var shared: InitialRouter!
  
  private var window: UIWindow
  
  required init(window: UIWindow) {
    self.window = window
  }
  
  func setup() {
    if let _ = Auth.auth().currentUser?.uid {
      window.rootViewController = BasicCoordinator<ConversationsRoute>(initialRoute: .conversations, initialLoadingType: .immediately).rootViewController
    } else {
      window.rootViewController = BasicCoordinator<LoginRoute>(initialRoute: .login, initialLoadingType: .immediately).rootViewController
    }
  }
  
}
