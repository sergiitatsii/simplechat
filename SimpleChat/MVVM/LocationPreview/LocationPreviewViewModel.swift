import CoreLocation
import MapKit

protocol LocationPreviewViewModelTypeInputs {
  var location: Variable<CLLocationCoordinate2D?> { get }
  var closeAction: CocoaAction { get }
}

protocol LocationPreviewViewModelTypeOuputs {
  var annotation: Driver<MKPointAnnotation> { get }
}

protocol LocationPreviewViewModelType {
  var inputs: LocationPreviewViewModelTypeInputs { get }
  var outputs: LocationPreviewViewModelTypeOuputs { get }
}

extension LocationPreviewViewModel: LocationPreviewViewModelTypeInputs, LocationPreviewViewModelTypeOuputs {
  var inputs: LocationPreviewViewModelTypeInputs { return self }
  var outputs: LocationPreviewViewModelTypeOuputs { return self }
}

class LocationPreviewViewModel: LocationPreviewViewModelType {
  
  // MARK: Inputs
  var location: Variable<CLLocationCoordinate2D?> = Variable(nil)
  
  lazy var closeAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .close).presentation
  }
  
  // MARK: Outputs
  var annotation: Driver<MKPointAnnotation>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       location: CLLocationCoordinate2D)
  {
    self.coordinator = coordinator
    self.location.value = location
    
    annotation = self.location
      .asDriver()
      .filterNil()
      .flatMap { coordinate in
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        return Observable.just(annotation).asDriver(onErrorJustReturn: MKPointAnnotation())
      }
  }
  
}
