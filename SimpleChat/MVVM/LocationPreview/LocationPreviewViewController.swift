import MapKit
import RxMKMapView
import FontAwesome_swift

class LocationPreviewViewController: ConfigurableViewController, BindableType {
  
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var closeButton: UIButton!
  
  // MARK: ViewModel
  var viewModel: LocationPreviewViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    
    let closeImage = UIImage.fontAwesomeIcon(
      name: .timesCircle,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: closeButton.bounds.size.width,
                   height: closeButton.bounds.size.height))
    closeButton.setImage(closeImage, for: .normal)
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    closeButton.rx.action = inputs.closeAction
    
    outputs.annotation
      .map{ $0.coordinate }
      .asDriver()
      .drive(mapView.rx.region)
      .disposed(by: disposeBag)
    
    outputs.annotation
      .map{ [$0] }
      .drive(mapView.rx.annotations)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
  
}
