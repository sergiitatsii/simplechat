import FontAwesome_swift
import RxKeyboard

class LoginViewController: ConfigurableViewController, BindableType {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var scrollBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var wrongInputLabel: UILabel!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var toRegistrationButton: UIButton!
  
  // MARK: ViewModel
  var viewModel: LoginViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    
    emailTextField.delegate = self
    passwordTextField.delegate = self
    
    logoImageView.image = UIImage.fontAwesomeIcon(
      name: .comments,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 120, height: 120))
    
    let gestureRecognizer = UITapGestureRecognizer()
    gestureRecognizer.numberOfTapsRequired = 1
    view.addGestureRecognizer(gestureRecognizer)
    gestureRecognizer.rx.event.asObservable()
      .subscribe({ [weak self] _ in
        self?.hideKeyboard()
      })
      .disposed(by: disposeBag)
    
    RxKeyboard.instance.drive(scrollView).disposed(by: rx.disposeBag)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    emailTextField.text = ""
    passwordTextField.text = ""
    wrongInputLabel.isHidden = true
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    emailTextField.rx.text.asDriver().drive(inputs.emailVariable).disposed(by: disposeBag)
    passwordTextField.rx.text.asDriver().drive(inputs.passwordVariable).disposed(by: disposeBag)
    
    outputs.credentialsValid
      .skip(1)
      .startWith(true)
      .drive(wrongInputLabel.rx.isHidden)
      .disposed(by: disposeBag)
    
    outputs.credentialsValid
      .drive(onNext: { [weak self] valid in
        self?.loginButton.isEnabled = valid
        self?.loginButton.alpha = valid ? 1.0 : 0.5
      })
      .disposed(by: disposeBag)
    
    outputs.signingIn
      .drive(rx.blockUI(message: "Signing in...".localized,
                        messageFont: UIFont.SimpleChat.billabong(50)))
      .disposed(by: disposeBag)
    
    outputs.signedIn
      .drive(onNext: { signedIn in
        print("User logged in = \(signedIn)")
      })
      .disposed(by: disposeBag)
    
    loginButton.rx.tap
      .subscribe(onNext: { [weak self] _ in
        self?.view.endEditing(true)
      })
      .disposed(by: disposeBag)
    
    loginButton.rx.action = inputs.loginAction
    toRegistrationButton.rx.action = inputs.toRegistrationAction
  }
  
  // MARK: Private functions
  fileprivate func hideKeyboard() {
    emailTextField.resignFirstResponder()
    passwordTextField.resignFirstResponder()
  }
  
}

// MARK: UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField === emailTextField { passwordTextField.becomeFirstResponder() }
    else { textField.resignFirstResponder() }
    return false
  }
  
}
