import RxSwiftUtilities

protocol RegistrationViewModelTypeInputs {
  var nameVariable: Variable<String> { get }
  var emailVariable: Variable<String> { get }
  var passwordVariable: Variable<String> { get }
  var passwordConfirmationVariable: Variable<String> { get }
  var registrationTrigger: InputSubject<Void> { get }
  var toLoginAction: CocoaAction { get }
}

protocol RegistrationViewModelTypeOuputs {
  var credentialsValid: Driver<Bool> { get }
  var avatarImage: Variable<UIImage> { get }
  var signingUp: Driver<Bool> { get }
}

protocol RegistrationViewModelType {
  var inputs: RegistrationViewModelTypeInputs { get }
  var outputs: RegistrationViewModelTypeOuputs { get }
}

class RegistrationViewModel: RegistrationViewModelTypeInputs, RegistrationViewModelTypeOuputs, RegistrationViewModelType {
  
  // MARK: RegistrationViewModelType
  var inputs: RegistrationViewModelTypeInputs { return self }
  var outputs: RegistrationViewModelTypeOuputs { return self }
  
  // MARK: Inputs
  let nameVariable: Variable<String> = Variable("")
  let emailVariable: Variable<String> = Variable("")
  let passwordVariable: Variable<String> = Variable("")
  let passwordConfirmationVariable: Variable<String> = Variable("")
  lazy var registrationTrigger: InputSubject<Void> = registartionAction.inputs
  lazy var toLoginAction = CocoaAction { [unowned self] in
    return self.coordinator.transition(to: .login).presentation
  }
  
  // MARK: Outputs
  let credentialsValid: Driver<Bool>
  var avatarImage: Variable<UIImage> = Variable(UIImage())
  let signingUp: Driver<Bool>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<RegistrationRote>
  private let service: UserServiceType
  private let activityIndicator = ActivityIndicator()
  private let attemptRegistration = PublishSubject<Void>()
  
  private lazy var registartionAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    self.attemptRegistration.onNext(())
    return .empty()
  }
  
  // MARK: Init
  init(coordinator: AnyCoordinator<RegistrationRote>,
       service: UserServiceType = UserService()) {
    self.coordinator = coordinator
    self.service = service
    
    let nameValid = nameVariable.asDriver()
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.utf8.count > 3 }

    let emailValid = emailVariable.asDriver()
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.isEmail }

    let passwordValid = passwordVariable.asDriver()
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.utf8.count > 5 }
    
    let passwordConfirmationValid = Driver.combineLatest(
      passwordConfirmationVariable.asDriver(),
      passwordVariable.asDriver(),
      resultSelector: validatePasswordConfirmation)
      
    credentialsValid = Driver.combineLatest(nameValid, emailValid, passwordValid, passwordConfirmationValid) { $0 && $1 && $2 && $3 }
    
    avatarImage.value = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 100, height: 100))
    
    signingUp = activityIndicator.asDriver()
    
    let credentials = Driver.combineLatest(
    nameVariable.asDriver(),
    emailVariable.asDriver(),
    passwordVariable.asDriver(),
    avatarImage.asDriver())
    { (name: $0, email: $1, password: $2, avatar: $3) }
    
    let signedUp = attemptRegistration.asDriver(onErrorJustReturn: ())
      .withLatestFrom(credentials)
      .flatMapLatest { [activityIndicator] credentials in
        return service.register(with: credentials.name,
                                email: credentials.email,
                                password: credentials.password,
                                avatar: credentials.avatar)
          .trackActivity(activityIndicator)
          .asDriver(onErrorJustReturn: Result(value: false))
      }
    
    signedUp
      .map { $0.value }
      .filterNil()
      .map { $0 == true }
      .drive(onNext: { _ in
        coordinator.transition(to: RegistrationRote.conversations)
      })
      .disposed(by: disposeBag)
    
    signedUp
      .map { $0.error }
      .filterNil()
      .drive(onNext: { error in
        coordinator.transition(to: RegistrationRote.alert(title: "Error".localized, message: error.localizedDescription))
      })
      .disposed(by: disposeBag)
  }
  
}

// MARK: Hepler function
fileprivate func validatePasswordConfirmation(_ confirmation: String, password: String) -> Bool {
  if confirmation.count == 0 { return false }
  if confirmation != password { return false }
  return true
}
