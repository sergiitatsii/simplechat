import FontAwesome_swift
import RxKeyboard
import Photos

enum PhotoSource {
  case library
  case camera
}

class RegistrationViewController: ConfigurableViewController, BindableType {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: RoundedImageView!
  @IBOutlet weak var changeAvatarButton: RoundedButton!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordConfirmationTextField: UITextField!
  @IBOutlet weak var wrongInputLabel: UILabel!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var toLoginButton: UIButton!
  @IBOutlet var allTextFields: [UITextField]!
  
  let imagePicker = UIImagePickerController()
  
  // MARK: ViewModel
  var viewModel: RegistrationViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    
    let gestureRecognizer = UITapGestureRecognizer()
    gestureRecognizer.numberOfTapsRequired = 1
    view.addGestureRecognizer(gestureRecognizer)
    gestureRecognizer.rx.event.asObservable()
      .subscribe({ [weak self] _ in
        self?.hideKeyboard()
      })
      .disposed(by: disposeBag)
    
    RxKeyboard.instance.drive(scrollView).disposed(by: rx.disposeBag)
    
    allTextFields.forEach { $0.delegate = self }
    
    imagePicker.delegate = self
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    nameTextField.rx.text.orEmpty.asDriver()
      .drive(inputs.nameVariable)
      .disposed(by: disposeBag)
    emailTextField.rx.text.orEmpty.asDriver()
      .drive(inputs.emailVariable)
      .disposed(by: disposeBag)
    passwordTextField.rx.text.orEmpty.asDriver()
      .drive(inputs.passwordVariable)
      .disposed(by: disposeBag)
    passwordConfirmationTextField.rx.text.orEmpty.asDriver()
      .drive(inputs.passwordConfirmationVariable)
      .disposed(by: disposeBag)
    
    outputs.credentialsValid.skip(1)
      .startWith(true)
      .drive(wrongInputLabel.rx.isHidden)
      .disposed(by: disposeBag)
    outputs.credentialsValid
      .drive(registerButton.rx.isEnabled)
      .disposed(by: disposeBag)
    outputs.credentialsValid
      .map{ $0 ? UIColor.SimpleChat.blue : UIColor.SimpleChat.gray }
      .drive(registerButton.rx.borderColor)
      .disposed(by: disposeBag)
    outputs.credentialsValid
      .map{ $0 ? UIColor.SimpleChat.blue : UIColor.SimpleChat.gray }
      .drive(registerButton.rx.titleColor)
      .disposed(by: disposeBag)
    outputs.avatarImage.asDriver()
      .drive(avatarImageView.rx.image)
      .disposed(by: disposeBag)
    outputs.signingUp
      .drive(rx.blockUI(message: "Registration...".localized,
                        messageFont: UIFont.SimpleChat.billabong(50)))
      .disposed(by: disposeBag)
    
    changeAvatarButton.rx.tap
      .subscribe({ [weak self] _ in
        self?.showActionSheet()
      })
      .disposed(by: disposeBag)
    
    registerButton.rx.tap
      .subscribe(onNext: { [weak self] _ in
        self?.view.endEditing(true)
      })
      .disposed(by: disposeBag)
    
    registerButton.rx.tap
      .bind(to: inputs.registrationTrigger)
      .disposed(by: disposeBag)
    
    toLoginButton.rx.action = inputs.toLoginAction
  }
  
  // MARK: Private functions
  fileprivate func hideKeyboard() {
    allTextFields.forEach { $0.resignFirstResponder() }
  }
  
  fileprivate func showActionSheet() {
    let actionSheet = UIAlertController(title: nil, message: "Select the source", preferredStyle: .actionSheet)
    let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
      (alert: UIAlertAction!) -> Void in
      self.openPhotoPickerWith(source: .camera)
    })
    let photoAction = UIAlertAction(title: "Gallery", style: .default, handler: {
      (alert: UIAlertAction!) -> Void in
      self.openPhotoPickerWith(source: .library)
    })
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      actionSheet.addAction(cameraAction)
    }
    actionSheet.addAction(photoAction)
    actionSheet.addAction(cancelAction)
    present(actionSheet, animated: true, completion: nil)
  }
  
  fileprivate func openPhotoPickerWith(source: PhotoSource) {
    switch source {
    case .camera:
      let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      if (status == .authorized || status == .notDetermined) {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
      }
    case .library:
      let status = PHPhotoLibrary.authorizationStatus()
      if (status == .authorized || status == .notDetermined) {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
      }
    }
  }
  
}

// MARK: UITextFieldDelegate
extension RegistrationViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    switch textField {
    case nameTextField:
      emailTextField.becomeFirstResponder()
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      passwordConfirmationTextField.becomeFirstResponder()
    default:
      passwordConfirmationTextField.resignFirstResponder()
    }
    
    return false
  }
  
}

// MARK: UIImagePickerControllerDelegate
extension RegistrationViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let pickedImage = info[.editedImage] as? UIImage {
      viewModel.outputs.avatarImage.value = pickedImage
    }
    picker.dismiss(animated: true, completion: nil)
  }
  
}
