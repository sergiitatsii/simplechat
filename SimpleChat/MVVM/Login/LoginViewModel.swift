import RxSwiftUtilities

protocol LoginViewModelTypeInputs {
  var emailVariable: Variable<String?> { get }
  var passwordVariable: Variable<String?> { get }
  var loginAction: CocoaAction { get }
  var toRegistrationAction: CocoaAction { get }
}

protocol LoginViewModelTypeOuputs {
  var credentialsValid: Driver<Bool> { get }
  var signingIn: Driver<Bool> { get }
  var signedIn: Driver<Bool> { get }
}

protocol LoginViewModelType {
  var inputs: LoginViewModelTypeInputs { get }
  var outputs: LoginViewModelTypeOuputs { get }
}

class LoginViewModel: LoginViewModelTypeInputs, LoginViewModelTypeOuputs, LoginViewModelType {
  
  // MARK: LoginViewModelType
  var inputs: LoginViewModelTypeInputs { return self }
  var outputs: LoginViewModelTypeOuputs { return self }
  
  // MARK: Inputs
  let emailVariable: Variable<String?> = Variable(nil)
  let passwordVariable: Variable<String?> = Variable(nil)
  
  lazy var loginAction: CocoaAction = {
    return CocoaAction(enabledIf: credentialsValid.asObservable()) { [weak self] in
      guard let `self` = self else { return .empty() }
      self.attemptLogin.onNext(())
      return .empty()
    }
  }()
  
  lazy var toRegistrationAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .registration).presentation
  }
  
  // MARK: Outputs
  let credentialsValid: Driver<Bool>
  let signingIn: Driver<Bool>
  let signedIn: Driver<Bool>
  
  // MARK: Private
  private let coordinator: AnyCoordinator<LoginRoute>
  private let service: UserServiceType
  private let activityIndicator = ActivityIndicator()
  private let attemptLogin = PublishSubject<Void>()
  
  // MARK: Init
  init(coordinator: AnyCoordinator<LoginRoute>,
       service: UserServiceType = UserService()) {
    self.coordinator = coordinator
    self.service = service
    
    let emailValid = emailVariable.asDriver()
      .map { $0 ?? "" }
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.isEmail }
    
    let passwordValid = passwordVariable.asDriver()
      .map { $0 ?? "" }
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.utf8.count > 5 }
    
    credentialsValid = Driver.combineLatest(emailValid, passwordValid) { $0 && $1 }
    signingIn = activityIndicator.asDriver()
    
    let credentials = Driver.combineLatest(
      emailVariable.asDriver(),
      passwordVariable.asDriver())
    { (email: $0, password: $1) }
    
    signedIn = attemptLogin.asDriver(onErrorJustReturn: ())
      .withLatestFrom(credentials)
      .flatMapLatest { [activityIndicator] credentials in
        return service.login(with: credentials.email ?? "",
                             password: credentials.password ?? "")
          .trackActivity(activityIndicator)
          .asDriver(onErrorJustReturn: false)
      }
      .flatMapLatest { loggedIn -> Driver<Bool> in
        let route = loggedIn ? LoginRoute.conversations : LoginRoute.alert(title: "Error".localized, message: "Login failed.".localized)
        
        return coordinator.transition(to: route).presentation
          .map { loggedIn }
          .asDriver(onErrorJustReturn: false)
    }
  }
  
}
