import FontAwesome_swift

class ImagePreviewViewController: ConfigurableViewController, BindableType {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var closeButton: UIButton!
  
  // MARK: ViewModel
  var viewModel: ImagePreviewViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    scrollView.minimumZoomScale = 1.0
    scrollView.maximumZoomScale = 3.0
    
    let closeImage = UIImage.fontAwesomeIcon(
      name: .timesCircle,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: closeButton.bounds.size.width,
                   height: closeButton.bounds.size.height))
    closeButton.setImage(closeImage, for: .normal)
    
    let gestureRecognizer = UITapGestureRecognizer()
    gestureRecognizer.numberOfTapsRequired = 2
    scrollView.addGestureRecognizer(gestureRecognizer)
    gestureRecognizer.rx.event.asDriver()
      .drive(onNext: { [weak self] recognizer in
        self?.handleGesture(from: recognizer)
      })
      .disposed(by: disposeBag)
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    //let outputs = viewModel.outputs
    
    closeButton.rx.action = inputs.closeAction
    
    inputs.image.asDriver()
      .filterNil()
      .drive(imageView.rx.image)
      .disposed(by: disposeBag)
    
    Variable(imageView.frame.size).asDriver()
      .drive(scrollView.rx.contentSize)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
  private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
    var zoomRect = CGRect.zero
    zoomRect.size.height = imageView.frame.size.height / scale
    zoomRect.size.width = imageView.frame.size.width / scale
    let newCenter = imageView.convert(center, from: scrollView)
    zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
    zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
    return zoomRect
  }
  
  private func handleGesture(from recognizer: UITapGestureRecognizer) {
    if scrollView.zoomScale == 1 {
      scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
    } else {
      scrollView.setZoomScale(1, animated: true)
    }
  }
  
}

// MARK: UIScrollViewDelegate
extension ImagePreviewViewController: UIScrollViewDelegate {
  
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  
}
