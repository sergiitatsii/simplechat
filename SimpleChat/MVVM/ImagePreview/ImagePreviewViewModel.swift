protocol ImagePreviewViewModelTypeInputs {
  var image: Variable<UIImage?> { get }
  var closeAction: CocoaAction { get }
}

protocol ImagePreviewViewModelTypeOuputs {
  
}

protocol ImagePreviewViewModelType {
  var inputs: ImagePreviewViewModelTypeInputs { get }
  var outputs: ImagePreviewViewModelTypeOuputs { get }
}

extension ImagePreviewViewModel: ImagePreviewViewModelTypeInputs, ImagePreviewViewModelTypeOuputs {
  var inputs: ImagePreviewViewModelTypeInputs { return self }
  var outputs: ImagePreviewViewModelTypeOuputs { return self }
}

class ImagePreviewViewModel: ImagePreviewViewModelType {
  
  // MARK: Inputs
  var image: Variable<UIImage?> = Variable(nil)
  
  lazy var closeAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .close).presentation
  }
  
  // MARK: Outputs
  
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       image: UIImage)
  {
    self.coordinator = coordinator
    self.image.value = image
  }
  
}
