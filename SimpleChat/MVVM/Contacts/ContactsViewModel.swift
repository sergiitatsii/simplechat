import RxSwiftUtilities
import RxCoordinator
import FirebaseAuth

protocol ContactsViewModelTypeInputs {
  var closeAction: CocoaAction { get }
  func refresh()
  func openConversation(with user: User)
}

protocol ContactsViewModelTypeOuputs {
  var isRefreshing: Driver<Bool> { get }
  var users: Variable<[User]> { get }
}

protocol ContactsViewModelType {
  var inputs: ContactsViewModelTypeInputs { get }
  var outputs: ContactsViewModelTypeOuputs { get }
}

extension ContactsViewModel: ContactsViewModelTypeInputs, ContactsViewModelTypeOuputs {
  var inputs: ContactsViewModelTypeInputs { return self }
  var outputs: ContactsViewModelTypeOuputs { return self }
}

class ContactsViewModel: ContactsViewModelType {
  
  // MARK: Inputs
  lazy var closeAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .close).presentation
  }
  
  func refresh() {
    refreshTrigger.onNext(())
  }
  
  func openConversation(with user: User) {
    coordinator.transition(to: .close)
    coordinator.transition(to: .chat(with: user))
  }
  
  // MARK: Outputs
  var users: Variable<[User]>
  var isRefreshing: Driver<Bool>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  private let service: UserServiceType
  private let activityIndicator = ActivityIndicator()
  private let refreshTrigger = PublishSubject<Void>()
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       service: UserServiceType = UserService())
  {
    self.coordinator = coordinator
    self.service = service
    
    isRefreshing = activityIndicator.asDriver()
    users = Variable([])
    
    guard let userID = Auth.auth().currentUser?.uid else { return }
    let result = refreshTrigger
      .asDriver(onErrorJustReturn: ())
      .flatMap { [activityIndicator] _ in
        return service.downloadAllUsers(except: userID)
          .trackActivity(activityIndicator)
          .asDriver(onErrorJustReturn: Result(value: []))
    }
    
    result
      .map { $0.value }
      .filterNil()
      .drive(onNext: { [unowned self] conversations in
        self.users.value = conversations
      })
      .disposed(by: disposeBag)
    
    result
      .map { $0.error }
      .filterNil()
      .drive(onNext: { error in
        coordinator.transition(to: ConversationsRoute.alert(title: "Error".localized, message: error.localizedDescription))
      })
      .disposed(by: disposeBag)
  }
  
}
