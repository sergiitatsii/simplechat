import DTModelStorage
import Nuke

class ContactCell: UICollectionViewCell, ModelTransfer {
  
  @IBOutlet weak var avatarImageView: RoundedImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
  func update(with model: User) {
    let userIcon = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: avatarImageView.bounds.width,
                   height: avatarImageView.bounds.height))
    
    Nuke.loadImage(
      with: model.avatarURL,
      options: ImageLoadingOptions(
        placeholder: userIcon,
        transition: .fadeIn(duration: 0.3)
      ),
      into: avatarImageView
    )
    
    nameLabel.text = model.name
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    avatarImageView.image = nil
  }
  
}
