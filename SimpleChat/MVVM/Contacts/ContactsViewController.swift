import DTCollectionViewManager
import DTModelStorage
import FontAwesome_swift

class ContactsViewController: ConfigurableViewController, BindableType, DTCollectionViewManageable {
  
  @IBOutlet weak var collectionView: UICollectionView?
  @IBOutlet weak var noContentView: UIView!
  @IBOutlet weak var noContentImageView: UIImageView!
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  // MARK: ViewModel
  var viewModel: ContactsViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    collectionView?.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0)
    
    let noContentImage = UIImage.fontAwesomeIcon(
      name: .sadTear,
      style: .solid,
      textColor: UIColor.white,
      size: CGSize(width: 150, height: 150))
    
    noContentImageView.image = noContentImage
    
    manager.register(ContactCell.self)
    manager.startManaging(withDelegate: self)
    
    manager.didSelect(ContactCell.self) { [weak self] cell, model, indexPath in
      self?.viewModel.inputs.openConversation(with: model)
    }
    
    manager.collectionViewUpdater?.didUpdateContent = { [weak self] _ in
      self?.updateViews()
    }
    
    refresh()
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    closeButton.rx.action = inputs.closeAction
    
    outputs.isRefreshing
      .drive(activityIndicator.rx.animating)
      .disposed(by: disposeBag)
    
    outputs.users
      .asDriver()
      .drive(manager.memoryStorage.rx.items())
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
  @objc private func refresh() {
    viewModel.inputs.refresh()
  }
  
  private func updateViews() {
    if (manager.storage.sections.first?.numberOfItems ?? 0) > 0 {
      collectionView?.isHidden = false
      noContentView.isHidden = true
    } else {
      collectionView?.isHidden = true
      noContentView.isHidden = false
    }
  }
    
}

// MARK: UICollectionViewDelegateFlowLayout
extension ContactsViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize
  {
    if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
      let width = (0.3 * UIScreen.main.bounds.height)
      let height = width + 30
      return CGSize.init(width: width, height: height)
    } else {
      let width = (0.3 * UIScreen.main.bounds.width)
      let height = width + 30
      return CGSize.init(width: width, height: height)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
    
}
