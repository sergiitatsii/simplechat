import RxSwiftUtilities
import RxCoordinator
import FirebaseAuth

protocol ProfileViewModelTypeInputs {
  var logoutAction: CocoaAction { get }
  var closeAction: CocoaAction { get }
}

protocol ProfileViewModelTypeOuputs {
  var isLoading: Driver<Bool> { get }
  var user: Variable<User?> { get }
}

protocol ProfileViewModelType {
  var inputs: ProfileViewModelTypeInputs { get }
  var outputs: ProfileViewModelTypeOuputs { get }
}

extension ProfileViewModel: ProfileViewModelTypeInputs, ProfileViewModelTypeOuputs {
  var inputs: ProfileViewModelTypeInputs { return self }
  var outputs: ProfileViewModelTypeOuputs { return self }
}

class ProfileViewModel: ProfileViewModelType {
  
  // MARK: Inputs
  lazy var logoutAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    
    self.service.logout()
      .map { $0 == true }
      .subscribe(onNext: { _ in
        self.coordinator.transition(to: .logout)
      })
      .disposed(by: self.disposeBag)
    
    return .empty()
  }
  
  lazy var closeAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .close).presentation
  }
  
  // MARK: Outputs
  var isLoading: Driver<Bool>
  var user: Variable<User?>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  private let service: UserServiceType
  private let activityIndicator = ActivityIndicator()
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       service: UserServiceType = UserService())
  {
    self.coordinator = coordinator
    self.service = service
    
    isLoading = activityIndicator.asDriver()
    user = Variable(nil)
    
    if let userID = Auth.auth().currentUser?.uid {
      service.info(for: userID)
        .filterNil()
        .trackActivity(activityIndicator)
        .subscribe(onNext: { [unowned self] user in
          self.user.value = user
        }).disposed(by: disposeBag)
    }
  }
  
}
