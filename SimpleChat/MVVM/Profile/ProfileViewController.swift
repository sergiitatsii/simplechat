import FontAwesome_swift
import Nuke
import RxNuke

class ProfileViewController: ConfigurableViewController, BindableType {
  
  @IBOutlet weak var avatarImageView: RoundedImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var logoutButton: UIButton!
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  // MARK: ViewModel
  var viewModel: ProfileViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    
    let closeImage = UIImage.fontAwesomeIcon(
      name: .timesCircle,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 40, height: 40))
    closeButton.setImage(closeImage, for: .normal)
    
    let avatarImage = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: avatarImageView.bounds.width, height: avatarImageView.bounds.height))
    avatarImageView.image = avatarImage
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    logoutButton.rx.action = inputs.logoutAction
    closeButton.rx.action = inputs.closeAction
    
    outputs.isLoading
      .drive(activityIndicator.rx.animating)
      .disposed(by: disposeBag)
    
    outputs
      .user.asDriver().filterNil()
      .map { $0.avatarURL }
      .drive(onNext: { [unowned self] url in
        ImagePipeline.shared.rx.loadImage(with: url)
          .subscribe(onSuccess: { self.avatarImageView.image = $0.image })
          .disposed(by: self.disposeBag)
      })
      .disposed(by: disposeBag)
    
    outputs
      .user.asDriver().filterNil()
      .map { $0.name }
      .drive(nameLabel.rx.text)
      .disposed(by: disposeBag)
    
    outputs
      .user.asDriver().filterNil()
      .map { $0.email }
      .drive(emailLabel.rx.text)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
  
}
