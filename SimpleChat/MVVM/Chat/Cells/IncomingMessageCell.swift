import DTModelStorage
import Kingfisher

class IncomingMessageCell: UITableViewCell, ModelTransfer {
  
  @IBOutlet weak var avatarImageView: RoundedImageView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var messageTextView: UITextView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  func update(with model: IncomingMessage) {
    let userIcon = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: avatarImageView.bounds.width,
                   height: avatarImageView.bounds.height))
    avatarImageView.image = userIcon
    
    switch model.type {
    case .text:
      messageTextView.text = model.content as? String
    case .image:
      let imageIcon = UIImage.fontAwesomeIcon(
        name: .image,
        style: .regular,
        textColor: UIColor.SimpleChat.blue,
        size: CGSize(width: 200, height: 200))
      
      guard
        let urlString = model.content as? String,
        let url = URL(string: urlString)
      else {
        backgroundImageView.backgroundColor = UIColor.clear
        backgroundImageView.image = imageIcon
        messageTextView.isHidden = true
        return
      }
      
      activityIndicator.startAnimating()
      backgroundImageView
        .kf.setImage(with: url,
                     placeholder: imageIcon) { [weak self] _, _, _, _ in
                      self?.activityIndicator.stopAnimating()
      }
      
      backgroundImageView.backgroundColor = UIColor.clear
      messageTextView.isHidden = true
    case .location:
      let locationIcon = UIImage.fontAwesomeIcon(
        name: .mapMarkedAlt,
        style: .solid,
        textColor: UIColor.SimpleChat.blue,
        size: CGSize(width: 48, height: 36))
      
      backgroundImageView.backgroundColor = UIColor.clear
      backgroundImageView.image = locationIcon
      messageTextView.isHidden = true
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    backgroundImageView.layer.cornerRadius = 15
    backgroundImageView.clipsToBounds = true
    messageTextView.textContainerInset = UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    avatarImageView.image = nil
    backgroundImageView.backgroundColor = UIColor.SimpleChat.blue
    backgroundImageView.kf.cancelDownloadTask()
    backgroundImageView.image = nil
    messageTextView.isHidden = false
    messageTextView.text = nil
  }
  
}
