import CoreLocation
import RxSwiftUtilities
import Nuke

protocol ChatViewModelTypeInputs {
  func fetchMessages()
  func composeMessage(with type: MessageType, content: Any)
  func markMessagesAsRead()
  func openImagePreview(with image: UIImage)
  func openLocationPreview(with location: CLLocationCoordinate2D)
}

protocol ChatViewModelTypeOuputs {
  var isFetching: Driver<Bool> { get }
  var message: Driver<Message> { get }
}

protocol ChatViewModelType {
  var inputs: ChatViewModelTypeInputs { get }
  var outputs: ChatViewModelTypeOuputs { get }
}

extension ChatViewModel: ChatViewModelTypeInputs, ChatViewModelTypeOuputs {
  var inputs: ChatViewModelTypeInputs { return self }
  var outputs: ChatViewModelTypeOuputs { return self }
}

class ChatViewModel: ChatViewModelType {
  
  // MARK: Inputs
  var user: User
  
  func fetchMessages() {
    fetchTrigger.onNext(())
  }
  
  func composeMessage(with type: MessageType, content: Any) {
    let message = Message(type: type,
                          content: content,
                          owner: .receiver,
                          timestamp: Int(Date().timeIntervalSince1970),
                          isRead: false)
    
    service.send(message: message, to: user.id)
      .map { $0.value }
      .filterNil()
      .subscribe(onNext: { status in
        print("*** Message has composed and sent: \(status)")
      })
      .disposed(by: disposeBag)
    
  }
  
  func markMessagesAsRead() {
    service.markMessagesAsRead(for: user.id)
      .map { $0.value }
      .filterNil()
      .subscribe(onNext: { status in
        print("*** Messages have marked as read: \(status)")
      })
      .disposed(by: disposeBag)
  }
  
  func openImagePreview(with image: UIImage) {
    coordinator.transition(to: .imagePreview(with: image))
  }
  
  func openLocationPreview(with location: CLLocationCoordinate2D) {
    coordinator.transition(to: .locationPreview(with: location))
  }
  
  // MARK: Outputs
  var isFetching: Driver<Bool>
  var message: Driver<Message>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  private let service: MessageServiceType
  private let activityIndicator = ActivityIndicator()
  private let fetchTrigger = PublishSubject<Void>()
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       service: MessageServiceType = MessageService(),
       user: User)
  {
    self.coordinator = coordinator
    self.service = service
    ImagePipeline.shared.loadImage(with: user.avatarURL, progress: nil) { response, error in
      user.avatar = response?.image
    }
    self.user = user
    
    isFetching = activityIndicator.asDriver()
    
    let result = fetchTrigger
      .asDriver(onErrorJustReturn: ())
      .flatMap { [activityIndicator] _ in
        return service.downloadAllMessages(for: user.id)
          .trackActivity(activityIndicator)
          .asDriver(onErrorJustReturn:
            Result(error: .downloadAllMessagesFailure("Conversion observable sequence to Driver failed.")))
    }
    
    message = result
      .map { $0.value }
      .filterNil()
    
    result
      .map { $0.error }
      .filterNil()
      .drive(onNext: { error in
        print("CHAT ERROR: \(error.localizedDescription)")
        //coordinator.transition(to: ConversationsRoute.alert(title: "Error".localized, message: error.localizedDescription))
      })
      .disposed(by: disposeBag)
  }
  
}
