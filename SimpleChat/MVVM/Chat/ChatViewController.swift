import DTTableViewManager
import DTModelStorage
import RxKeyboard
import Photos
import CoreLocation
import FontAwesome_swift

class ChatViewController: ConfigurableViewController, BindableType, DTTableViewManageable {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet var inputBar: UIView!
  @IBOutlet weak var moreOptionsButton: UIButton!
  @IBOutlet weak var sendMessageButton: UIButton!
  @IBOutlet weak var inputTextField: UITextField!
  @IBOutlet weak var selectTextButton: UIButton!
  @IBOutlet weak var selectPictureButton: UIButton!
  @IBOutlet weak var selectCameraButton: UIButton!
  @IBOutlet weak var selectLocationButton: UIButton!
  @IBOutlet weak var inputBarBottomConstraint: NSLayoutConstraint!
    
  override var inputAccessoryView: UIView? {
    get {
      inputBar.frame.size.height = barHeight
      inputBar.clipsToBounds = true
      return inputBar
    }
  }
  
  override var canBecomeFirstResponder: Bool {
    return true
  }
  
  let imagePicker = UIImagePickerController()
  let locationManager = CLLocationManager()
  
  // MARK: ViewModel
  var viewModel: ChatViewModel!
  
  private let barHeight: CGFloat = 50
  private var canSendLocation = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
    customization()
    tableSetup()
    inputBarButtonsSetup()
    keyboardSetup()
    
    imagePicker.delegate = self
    locationManager.delegate = self
    
    inputTextField.rx.controlEvent(.editingDidEndOnExit)
      .subscribe()
      .disposed(by: disposeBag)
    
    viewModel.inputs.fetchMessages()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.viewModel.markMessagesAsRead()
  }
  
  // MARK: BindableType
  func bindViewModel() {
    //let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    outputs.message
      .drive(onNext: { [weak self] message in
        self?.manager.memoryStorage.addItem(message)
      })
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
  private func customization() {
    configuration = .navigationBarVisible
    title = viewModel.user.name
    tableView.estimatedRowHeight = barHeight
    tableView.rowHeight = UITableView.automaticDimension
    tableView.contentInset.bottom = barHeight
    tableView.scrollIndicatorInsets.bottom = barHeight
    inputBar.backgroundColor = UIColor.clear
    selectCameraButton.isHidden = !UIImagePickerController.isSourceTypeAvailable(.camera)
    
    let moreOptionsImage = UIImage.fontAwesomeIcon(
      name: .thLarge,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    moreOptionsButton.setImage(moreOptionsImage, for: .normal)
    
    let sendMessageImage = UIImage.fontAwesomeIcon(
      name: .telegramPlane,
      style: .brands,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    sendMessageButton.setImage(sendMessageImage, for: .normal)
    
    let selectTextImage = UIImage.fontAwesomeIcon(
      name: .commentAlt,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    selectTextButton.setImage(selectTextImage, for: .normal)
    
    let selectPictureImage = UIImage.fontAwesomeIcon(
      name: .image,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    selectPictureButton.setImage(selectPictureImage, for: .normal)
    
    let selectCameraImage = UIImage.fontAwesomeIcon(
      name: .camera,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    selectCameraButton.setImage(selectCameraImage, for: .normal)
    
    let selectLocationImage = UIImage.fontAwesomeIcon(
      name: .mapMarkerAlt,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: 32, height: 25))
    selectLocationButton.setImage(selectLocationImage, for: .normal)
  }
  
  private func tableSetup() {
    manager.register(IncomingMessageCell.self)
    manager.register(OutcomingMessageCell.self)
    manager.startManaging(withDelegate: self)
    
    manager.configure(IncomingMessageCell.self) { [weak self] cell, model, _ in
      guard let avatar = self?.viewModel.user.avatar else { return }
      cell.avatarImageView.image = avatar
    }
    
    manager.didSelect(IncomingMessageCell.self) { [weak self] cell, model, indexPath in
      switch model.type {
      case .image:
        guard let image = cell.backgroundImageView.image else { return }
        self?.viewModel.inputs.openImagePreview(with: image)
      case .location:
        guard let locationString = model.content as? String else { return }
        let coordinates = locationString.components(separatedBy: ":")
        guard
          let latitude = CLLocationDegrees(coordinates[0]),
          let longitude = CLLocationDegrees(coordinates[1])
        else { return }
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self?.viewModel.inputs.openLocationPreview(with: location)
      default:
        break
      }
    }
    
    manager.didSelect(OutcomingMessageCell.self) { [weak self] cell, model, indexPath in
      switch model.type {
      case .image:
        guard let image = cell.backgroundImageView.image else { return }
        self?.viewModel.inputs.openImagePreview(with: image)
      case .location:
        guard let locationString = model.content as? String else { return }
        let coordinates = locationString.components(separatedBy: ":")
        guard
          let latitude = CLLocationDegrees(coordinates[0]),
          let longitude = CLLocationDegrees(coordinates[1])
          else { return }
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self?.viewModel.inputs.openLocationPreview(with: location)
      default:
        break
      }
    }
    
    manager.tableViewUpdater?.didUpdateContent = { [weak self] _ in
      guard let itemsCount = self?.manager.storage.sections.first?.numberOfItems else { return }
      if itemsCount > 0 {
        self?.tableView.scrollToRow(at: IndexPath(row: itemsCount - 1, section: 0), at: .bottom, animated: false)
        self?.viewModel.inputs.markMessagesAsRead()
      }
    }
  }
  
  private func inputBarButtonsSetup() {
    moreOptionsButton.rx.tap
      .subscribe({ [weak self] _ in
        self?.animateExtraButtons(toHide: false)
      })
      .disposed(by: disposeBag)
    
    let inputValid = inputTextField.rx.text.orEmpty
      .asDriver()
      .throttle(0.3)
      .distinctUntilChanged()
      .map { $0.utf8.count > 0 }
    
    inputValid
      .drive(sendMessageButton.rx.isEnabled)
      .disposed(by: disposeBag)
    
    sendMessageButton.rx.tap
      .withLatestFrom(inputValid)
      .filter{ $0 }
      .subscribe(onNext: { [weak self] _ in
        guard let `self` = self else { return }
        self.viewModel.composeMessage(with: .text, content: self.inputTextField.text!)
        self.inputTextField.text = ""
        self.sendMessageButton.isEnabled = false
      })
      .disposed(by: self.disposeBag)
    
    selectTextButton.rx.tap
      .subscribe({ [weak self] _ in
        self?.animateExtraButtons(toHide: true)
      })
      .disposed(by: disposeBag)
    
    selectPictureButton.rx.tap
      .subscribe({ [weak self] _ in
        self?.animateExtraButtons(toHide: true)
        self?.openImagePickerWith(source: .library)
      })
      .disposed(by: disposeBag)
    
    selectCameraButton.rx.tap
      .subscribe({ [weak self] _ in
        self?.animateExtraButtons(toHide: true)
        self?.openImagePickerWith(source: .camera)
      })
      .disposed(by: disposeBag)
    
    selectLocationButton.rx.tap
      .subscribe({ [weak self] _ in
        guard let `self` = self else { return }
        self.animateExtraButtons(toHide: true)
        self.canSendLocation = true
        if self.checkLocationPermission() {
          self.locationManager.startUpdatingLocation()
        } else {
          self.locationManager.requestWhenInUseAuthorization()
        }
      })
      .disposed(by: disposeBag)
  }
  
  private func keyboardSetup() {
    RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] in
      guard let `self` = self else { return }
      self.tableView.contentInset.bottom = $0
      self.tableView.scrollIndicatorInsets.bottom = $0
      
      guard let itemsCount = self.manager.storage.sections.first?.numberOfItems else { return }
      if itemsCount > 0 {
        self.tableView.scrollToRow(at: IndexPath(row: itemsCount - 1, section: 0), at: .bottom, animated: true)
      }
    })
    .disposed(by: rx.disposeBag)
  }
  
  private func animateExtraButtons(toHide: Bool)  {
    switch toHide {
    case true:
      inputBarBottomConstraint.constant = 0
      UIView.animate(withDuration: 0.3) {
        self.inputBar.layoutIfNeeded()
      }
    default:
      inputBarBottomConstraint.constant = -50
      UIView.animate(withDuration: 0.3) {
        self.inputBar.layoutIfNeeded()
      }
    }
  }
  
  private func openImagePickerWith(source: PhotoSource) {
    switch source {
    case .camera:
      let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      if (status == .authorized || status == .notDetermined) {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
      }
    case .library:
      let status = PHPhotoLibrary.authorizationStatus()
      if (status == .authorized || status == .notDetermined) {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
      }
    }
  }
  
  private func checkLocationPermission() -> Bool {
    var state = false
    switch CLLocationManager.authorizationStatus() {
    case .authorizedWhenInUse:
      state = true
    case .authorizedAlways:
      state = true
    default: break
    }
    return state
  }
  
}

// MARK: UITableViewDelegate
extension ChatViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if tableView.isDragging {
      cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
      UIView.animate(withDuration: 0.3, animations: {
        cell.transform = CGAffineTransform.identity
      })
    }
  }
  
}

// MARK: UIImagePickerControllerDelegate
extension ChatViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let pickedImage = info[.editedImage] as? UIImage {
      viewModel.inputs.composeMessage(with: .image, content: pickedImage)
    } else {
      let pickedImage = info[.originalImage] as! UIImage
      viewModel.inputs.composeMessage(with: .image, content: pickedImage)
    }
    picker.dismiss(animated: true, completion: nil)
  }
  
}

// MARK: CLLocationManagerDelegate
extension ChatViewController: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    locationManager.stopUpdatingLocation()
    if let lastLocation = locations.last {
      if canSendLocation {
        let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
        viewModel.inputs.composeMessage(with: .location, content: coordinate)
        canSendLocation = false
      }
    }
  }
  
}
