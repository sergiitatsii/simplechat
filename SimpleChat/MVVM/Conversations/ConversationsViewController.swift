import DTTableViewManager
import DTModelStorage
import FontAwesome_swift

class ConversationsViewController: ConfigurableViewController, BindableType, DTTableViewManageable {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var noContentView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  lazy var leftButton: RoundedBorderBarButtonItem = {
    let image = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.white,
      size: CGSize(width: 30, height: 30))
    
    let button = RoundedBorderBarButtonItem(radius: 15, image: image, target: self, action: nil)
    return button
  }()
  
  lazy var rightButton: UIBarButtonItem = {
    let image = UIImage.fontAwesomeIcon(
      name: .pen,
      style: .solid,
      textColor: UIColor.white,
      size: CGSize(width: 30, height: 30))
    
    let button = UIBarButtonItem(image: image, style: .plain, target: self, action: nil)
    return button
  }()
  
  //private var refreshControl: UIRefreshControl!
  
  // MARK: ViewModel
  var viewModel: ConversationsViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarVisible
    title = "Conversations".localized
    navigationItem.leftBarButtonItem = leftButton
    navigationItem.rightBarButtonItem = rightButton
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    manager.register(ConversationCell.self)
    manager.startManaging(withDelegate: self)
    
    manager.didSelect(ConversationCell.self) { [weak self] cell, model, indexPath in
      self?.tableView.deselectRow(at: indexPath, animated: true)
      self?.viewModel.openConversation(with: model.user)
    }
    
    manager.tableViewUpdater?.didUpdateContent = { [weak self] _ in
      self?.updateViews()
    }
    
    //configureRefreshControl()
    refresh()
  }
  
  // MARK: BindableType
  func bindViewModel() {
    let inputs = viewModel.inputs
    let outputs = viewModel.outputs
    
    leftButton.button.rx.action = inputs.showUserProfileAction
    rightButton.rx.action = inputs.showContactsAction
    
//    outputs.isRefreshing
//      .drive(refreshControl.rx.isRefreshing)
//      .disposed(by: disposeBag)
    
//    outputs.isRefreshing
//      .drive(activityIndicator.rx.animating)
//      .disposed(by: disposeBag)
    
    outputs.conversations
      .asDriver()
      .drive(manager.memoryStorage.rx.items())
      .disposed(by: disposeBag)
    
    outputs.avatar
      .asDriver()
      .drive(leftButton.rx.image)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private functions
//  private func configureRefreshControl() {
//    refreshControl = UIRefreshControl()
//    refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
//    tableView.addSubview(refreshControl)
//  }
  
  @objc private func refresh() {
    viewModel.inputs.refresh()
  }
  
  private func updateViews() {
    if (manager.storage.sections.first?.numberOfItems ?? 0) > 0 {
      tableView.isHidden = false
      noContentView.isHidden = true
    } else {
      tableView.isHidden = true
      noContentView.isHidden = false
    }
  }
  
}
