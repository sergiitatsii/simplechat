import RxSwiftUtilities
import RxCoordinator
import FirebaseAuth
import Nuke
import RxNuke

protocol ConversationsViewModelTypeInputs {
  var showUserProfileAction: CocoaAction { get }
  var showContactsAction: CocoaAction { get }
  func refresh()
}

protocol ConversationsViewModelTypeOuputs {
  var isRefreshing: Driver<Bool> { get }
  var conversations: Variable<[Conversation]> { get }
  var user: Variable<User?> { get }
  var avatar: Variable<UIImage> { get }
}

protocol ConversationsViewModelType {
  var inputs: ConversationsViewModelTypeInputs { get }
  var outputs: ConversationsViewModelTypeOuputs { get }
}

extension ConversationsViewModel: ConversationsViewModelTypeInputs, ConversationsViewModelTypeOuputs {
  var inputs: ConversationsViewModelTypeInputs { return self }
  var outputs: ConversationsViewModelTypeOuputs { return self }
}

class ConversationsViewModel: ConversationsViewModelType {
  
  // MARK: Inputs
  lazy var showUserProfileAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .userProfile).presentation
  }
  
  lazy var showContactsAction = CocoaAction { [weak self] in
    guard let `self` = self else { return .empty() }
    return self.coordinator.transition(to: .contacts).presentation
  }
  
  func refresh() {
    refreshTrigger.onNext(())
  }
  
  func openConversation(with user: User) {
    coordinator.transition(to: .chat(with: user))
  }
  
  // MARK: Outputs
  var conversations: Variable<[Conversation]>
  var isRefreshing: Driver<Bool>
  var user: Variable<User?>
  var avatar: Variable<UIImage>
  
  // MARK: Private
  private let disposeBag = DisposeBag()
  private let coordinator: AnyCoordinator<ConversationsRoute>
  private let conversationService: ConversationServiceType
  private let userService: UserServiceType
  private let activityIndicator = ActivityIndicator()
  private let refreshTrigger = PublishSubject<Void>()
  
  // MARK: Init
  init(coordinator: AnyCoordinator<ConversationsRoute>,
       conversationService: ConversationServiceType = ConversationService(),
       userService: UserServiceType = UserService())
  {
    self.coordinator = coordinator
    self.conversationService = conversationService
    self.userService = userService
    
    isRefreshing = activityIndicator.asDriver()
    conversations = Variable([])
    user = Variable(nil)
    avatar = Variable(UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.white,
      size: CGSize(width: 30, height: 30)))
    
    if let userID = Auth.auth().currentUser?.uid {
      userService.info(for: userID)
        .filterNil()
        .subscribe(onNext: { [unowned self] user in
          self.user.value = user
          ImagePipeline.shared.rx.loadImage(with: user.avatarURL)
            .subscribe(onSuccess: { self.avatar.value = $0.image.withRenderingMode(.alwaysOriginal) })
            .disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
    }
    
    let result = refreshTrigger
      .asDriver(onErrorJustReturn: ())
      .flatMap { [activityIndicator] _ in
        return conversationService.downloadAllConversations()
          .trackActivity(activityIndicator)
          .asDriver(onErrorJustReturn: Result(error: .downloadAllMessagesFailure("Conversion observable sequence to Driver failed.")))
    }
    
    result
      .map { $0.value }
      .filterNil()
      .drive(onNext: { [unowned self] conversation in
        var items = self.conversations.value
        if let index = items.firstIndex(of: conversation) {
          items[index] = conversation
        } else {
          items.append(conversation)
        }
        items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
        self.conversations.value = items
      })
      .disposed(by: disposeBag)
    
    result
      .map { $0.error }
      .filterNil()
      .drive(onNext: { error in
        print("CONVERSATIONS ERROR: \(error.localizedDescription)")
        //coordinator.transition(to: ConversationsRoute.alert(title: "Error".localized, message: error.localizedDescription))
      })
      .disposed(by: disposeBag)
  }
  
}
