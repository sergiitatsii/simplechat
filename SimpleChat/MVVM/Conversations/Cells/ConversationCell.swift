import DTModelStorage
import FontAwesome_swift
import Nuke

class ConversationCell: UITableViewCell, ModelTransfer {
  
  @IBOutlet weak var avatarImageView: RoundedImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var messageImageView: UIImageView!
  @IBOutlet weak var timeLabel: UILabel!
  
  func update(with model: Conversation) {
    let userIcon = UIImage.fontAwesomeIcon(
      name: .user,
      style: .solid,
      textColor: UIColor.SimpleChat.blue,
      size: CGSize(width: avatarImageView.bounds.width,
                   height: avatarImageView.bounds.height))
    
    Nuke.loadImage(
      with: model.user.avatarURL,
      options: ImageLoadingOptions(
        placeholder: userIcon,
        transition: .fadeIn(duration: 0.3)
      ),
      into: avatarImageView
    )
    
    switch model.lastMessage.type {
    case .image:
      let imageIcon = UIImage.fontAwesomeIcon(
        name: .image,
        style: .regular,
        textColor: UIColor.SimpleChat.gray,
        size: CGSize(width: messageImageView.bounds.size.width,
                     height: messageImageView.bounds.size.height))
      
      messageLabel.isHidden = true
      messageImageView.image = imageIcon
      messageImageView.isHidden = false
    case .location:
      let locationIcon = UIImage.fontAwesomeIcon(
        name: .mapMarkedAlt,
        style: .solid,
        textColor: UIColor.SimpleChat.gray,
        size: CGSize(width: messageImageView.bounds.size.width,
                     height: messageImageView.bounds.size.height))
      
      messageLabel.isHidden = true
      messageImageView.image = locationIcon
      messageImageView.isHidden = false
    default:
      messageLabel.text = model.lastMessage.content as? String
    }
    
    nameLabel.text = model.user.name
    let messageDate = Date(timeIntervalSince1970: TimeInterval(model.lastMessage.timestamp))
    timeLabel.text = messageDate.timeString
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    avatarImageView.image = nil
    messageLabel.isHidden = false
    messageImageView.image = nil
    messageImageView.isHidden = true
  }
  
}
