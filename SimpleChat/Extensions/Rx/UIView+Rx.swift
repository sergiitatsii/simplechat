extension Reactive where Base: UIView {
  
  public var borderColor: Binder<UIColor> {
    return Binder(base) { base, value in
      base.layer.borderColor = value.cgColor
    }
  }
  
}
