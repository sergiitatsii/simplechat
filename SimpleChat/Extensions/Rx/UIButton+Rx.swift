extension Reactive where Base: UIButton {
  
  public var titleColor: Binder<UIColor> {
    return Binder(base) { base, value in
      base.setTitleColor(value, for: .normal)
    }
  }
  
}

extension Reactive where Base: UIBarButtonItem {
  
  var image: Binder<UIImage> {
    return Binder(base) { base, value in
      base.image = value
    }
  }
  
}
