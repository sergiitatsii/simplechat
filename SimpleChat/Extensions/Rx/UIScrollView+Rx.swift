extension Reactive where Base: UIScrollView {
  
  public var contentSize: Binder<CGSize> {
    return Binder(base) { base, value in
      base.contentSize = value
    }
  }
  
}
