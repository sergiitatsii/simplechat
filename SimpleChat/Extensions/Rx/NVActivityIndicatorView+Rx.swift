import NVActivityIndicatorView

extension Reactive where Base: NVActivityIndicatorView {
  
  public var animating: Binder<Bool> {
    return Binder(base) { base, value in
      if value == base.isAnimating { return }
      if value { base.startAnimating() }
      else { base.stopAnimating() }
    }
  }
  
}

extension UIViewController: NVActivityIndicatorViewable {}
extension Reactive where Base: UIViewController {
  
  public func blockUI(with size: CGSize? = nil,
                      message: String? = nil,
                      messageFont: UIFont? = nil,
                      type: NVActivityIndicatorType? = nil,
                      color: UIColor? = nil,
                      padding: CGFloat? = nil,
                      displayTimeThreshold: Int? = nil,
                      minimumDisplayTime: Int? = nil,
                      backgroundColor: UIColor? = nil,
                      textColor: UIColor? = nil,
                      fadeInAnimation: FadeInAnimation? = NVActivityIndicatorView.DEFAULT_FADE_IN_ANIMATION) -> Binder<Bool> {
    return Binder<Bool>(base) { base, value in
      if value == base.isAnimating { return }
      if value { base.startAnimating(size,
                                     message: message,
                                     messageFont: messageFont,
                                     type: type,
                                     color: color,
                                     padding: padding,
                                     displayTimeThreshold: displayTimeThreshold,
                                     minimumDisplayTime: minimumDisplayTime,
                                     backgroundColor: backgroundColor,
                                     textColor: textColor,
                                     fadeInAnimation: fadeInAnimation) }
      else { base.stopAnimating() }
    }
  }
  
}
