import RxKeyboard

extension RxKeyboard {
  
  func drive(_ bottom: NSLayoutConstraint?, _ view: UIView?) -> Disposable {
    return RxKeyboard.instance.visibleHeight.drive(onNext: { [weak bottom, weak view] in
      bottom?.constant = $0
      view?.setNeedsLayout()
      UIView.animate(withDuration: 0) {
        view?.layoutIfNeeded()
      }
    })
  }
  
  func drive(_ scrollView: UIScrollView?) -> Disposable {
    return RxKeyboard.instance.visibleHeight.drive(onNext: { [weak scrollView] in
      scrollView?.contentInset.bottom = $0
      scrollView?.scrollIndicatorInsets.bottom = $0
    })
  }
  
}
