import DTTableViewManager
import DTModelStorage

extension Reactive where Base: MemoryStorage {
  
  public func items<T>(forSection section: Int = 0) -> Binder<[T]> {
    return Binder(base) { base, value in
      base.setItems(value, forSection: section)
    }
  }
  
}
