extension Date {
  
  static let iso8601Formatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
    //formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
    return formatter
  }()
  
  static var messageTimeFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    formatter.doesRelativeDateFormatting = true
    return formatter
  }()
  
  var iso8601: String { return Date.iso8601Formatter.string(from: self) }
  var timeString: String { return Date.messageTimeFormatter.string(from: self) }
  
}

extension String {
  
  var dateFromISO8601: Date? {
    return Date.iso8601Formatter.date(from: self)
  }
  
}
