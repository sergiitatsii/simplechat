extension UIFont {
  
  enum SimpleChat {
    static func billabong(_ size: CGFloat) -> UIFont {
      return UIFont(name: "Billabong", size: size) ?? UIFont.systemFont(ofSize: size)
    }
  }
  
}
